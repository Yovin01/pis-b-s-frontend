import React, { Component } from "react";
import Chart from "react-apexcharts";
import _ from 'lodash';

const URL = "https://computacion.unl.edu.ec/uv/api/medicionFechas";

const obtenerFechaHoyEcuador = () => {
    const fechaHoy = new Date();
    fechaHoy.setUTCHours(fechaHoy.getUTCHours() - 5);
    return fechaHoy.toISOString().split('T')[0];
};

const obtenerFechaMananaEcuador = () => {
    const fechaManana = new Date();
    fechaManana.setDate(fechaManana.getDate() + 1);
    fechaManana.setUTCHours(fechaManana.getUTCHours() - 5);
    return fechaManana.toISOString().split('T')[0];
};

class Grafica extends Component {

    constructor(props) {
        super(props);

        this.state = {
            options: {
                series: [
                    {
                        name: "Indice UV",
                        data: []
                    }
                ],
                chart: {
                    type: 'area',
                    height: 370,
                    width: "100%",
                    dropShadow: {
                        enabled: true,
                        color: '#000',
                        top: 0,
                        left: 7,
                        blur: 10,
                        opacity: 0.2
                    },
                  },
                colors: ['#0C2840'],
                    
                title: {
                    text: 'Indice UV en ' + this.props.nombre,
                    align: 'center'
                },
                grid: {
                    borderColor: '#e7e7e7',
                    row: {
                        colors: ['#f3f3f3', 'transparent'],
                        opacity: 0.5
                    },
                },
                markers: {
                    size: 1
                },
                xaxis: {
                    categories: [],
                    title: {
                        text: 'Horas del día'
                    }
                },
                yaxis: {
                    title: {
                        text: 'Indice UV'
                    },
                },
                legend: {
            
                    horizontalAlign: 'center',
                    floating: true,
                    offsetY: -5,
                    offsetX: -5
                },
            },
            series: [
                {
                    name: "Indice UV",
                    data: [],
                },
            ],
        };
    }

    componentDidMount() {
        const dispositivoId = parseInt(this.props.dispositivoId, 10);
        const data = {
            fechaInicio: obtenerFechaHoyEcuador(),
            fechaFin: obtenerFechaMananaEcuador()
        };

        fetch(`${URL}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-api-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOiJCQUNLRU5EIiwiaWF0IjoxNzA3MDc2NzkyfQ.xT3uhWgUbCsCFmPepyiGQOUIsdzetlfgrHvdGqKr-Iw"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => {
                const medicionesDispositivo = data.mediciones.filter(
                    (medicion) => medicion.dispositivoId === dispositivoId
                );

                const medicionesPorHora = _.groupBy(
                    medicionesDispositivo,
                    (medicion) => {
                        const fecha = new Date(medicion.fecha);
                        const hora = fecha.getHours() + 5;

                        if (hora >= 6 && hora <= 17) {
                            return (
                                (hora) +
                                ":" +
                                ("0" + fecha.getMinutes()).slice(-2)
                            );
                        } else if (hora < 6) {
                            return (
                                (hora) +
                                ":" +
                                ("0" + fecha.getMinutes()).slice(-2)
                            );
                        }
                    }
                );

                const promediosPorHora = _.map(medicionesPorHora, (mediciones, hora) => {
                    const horaInt = parseInt(hora.split(':')[0], 10);
                    if (horaInt >= 6 && horaInt <= 17) {
                        return {
                            hora,
                            promedioUV: _.meanBy(mediciones, 'uv'),
                        };
                    } else {
                        return null;
                    }
                });

                const promediosValidos = promediosPorHora.filter(item => item !== null);
                const horas = promediosValidos.map(item => item.hora);
                const medicionesUV = promediosValidos.map(item => item.promedioUV);

                this.setState({
                    options: {
                        ...this.state.options,
                        xaxis: {
                            title: {
                                text: 'Horas del día'
                            },
                            labels: {
                                formatter: function (value) {
                                    return value + " H";
                                },
                            },
                        },
                        yaxis: {
                            title: {
                                text: 'Indice UV',
                            },
                            labels: {
                                formatter: function (value) {
                                    return parseFloat(value.toFixed(2)) + " UV";
                                },
                            },
                        },
                    },
                    series: [
                        {
                            ...this.state.series[0],
                            data: medicionesUV.map(value => parseFloat(value.toFixed(2))),
                        },
                    ],
                });
            })
            .catch(error => console.error("Error al obtener datos:", error));
    }

    render() {
        return (
            <div className="appGary" style={{maxHeight:400}}>
            <div className="center-chartGary">
                <div className="mixed-chart">
                    <Chart
                        options={this.state.options}
                        series={this.state.series}
                        type="line"
                        width="100%"

                        height={this.state.options.chart.height}
                    />
                </div>
            </div>
        </div>
        );
    }
}

export default Grafica;
